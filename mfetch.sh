#!/usr/bin/env bash

[ "$TERM" = "linux" ] && exit

LINE[0]=$(uptime -p | sed -E 's/^up //; s/ ([ymwdh])\w+\b,?/\1/g; s/(m)(.+m)/M\2/')
LINE[1]=$(df -h | awk '/\/$/ {print $3 " / " $2}')
LINE[2]=$(df -h | awk '/\/home$/ {print $3 " / " $2}')
LINE[3]=$(free -h | awk '/^Mem/ {print $3 " / " $2 "\n"}' | sed 's/i//g')

HOST="$(hostname)" HOSTLEN=${#HOST} NAME=("Uptime" "Root" "Home" "Memory")
[ "$HOSTLEN" -gt 15 ] && HOST="$(printf "%.15s" "$HOST")"

while [ "$HOSTLEN" -gt 7 ]; do
	HOSTLEN=$(( HOSTLEN - 7 ))
done

F="\033[0;$(( HOSTLEN + 30 ))m"
A=("\033[1;36m" "\033[0;37m")
B=("\033[1;40;32m" "\033[0;40;37m")
R="\033[0m"

[ -n "$1" ] && case "$1" in
	"plain")
		F="$R";;
	"border")
		A='' B='';;
	"mono")
		A='' B='' F="$R";;
	"mono2")
		unset A B F R;;
esac

for ((i = 0; i < ${#LINE[@]}; i++)); do
	[ ${#LINE[$i]} -ge "${MAX:-0}" ] && MAX=${#LINE[$i]}
done

BASELEN=$(( 12 + MAX ))

printf "${F}%s%${BASELEN}s%s${R}\n" " ╔" "[${HOST}]" "╗" | sed 's/ /═/g; s/═/ /'

for ((i = 0; i < ${#LINE[@]}; i++)); do
	if [ -n "${LINE[$i]}" ]; then
		C=(${A[@]}) A=(${B[@]}) B=(${C[@]})
		printf " ${F}%s${C[0]} %-6s" "║" "${NAME[$i]}"
		printf "${C[1]}%$(( BASELEN - 8 ))s ${F}%s${R}\n" "${LINE[$i]}" "║"
	fi
done

printf "${F}%s%${BASELEN}s%s${R}\n\n" " ╚" " " "╝" | sed 's/ /═/g; s/═/ /'

command -v tmux >/dev/null && tmux ls 2>/dev/null | grep -cv "attached" | awk '{
	if ($0 == 1) print "There is 1 detached tmux session."
	else if ($0 > 1) print "There are " $0 " detached tmux sessions."
	}'
