# mfetch

Since I hop between similar ssh hosts, the colorful border tells me
where I've landed. It's based on hostname lenght and cycles through the
basic 8 ANSI colors, minus the black.

[Here](https://imgur.com/a/ETSeLeZ) are some screenshots.

## Install

You can get `mfetch-git` from the AUR.

## Usage

Append to bashrc:

| command    | behaviour  |
| :--        | --:        |
| `mfetch` | full color |
| `mfetch border` | colored border |
| `mfetch plain` | colorless border |
| `mfetch mono` | black and white |
| `mfetch mono2` | true monochrome |

## To do:

- [x] auto adjust width
- [ ] center "X / Y" type output on the separator
- [ ] add more info??
